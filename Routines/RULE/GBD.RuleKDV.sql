CREATE PROCEDURE GBD.RuleKDV( @StudyId INT, @PersonId INT ) AS
BEGIN
  EXEC dbo.RulePeriodicForm @StudyId,@PersonId,'KDV',180,2;
END;
