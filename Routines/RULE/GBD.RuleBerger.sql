CREATE PROCEDURE GBD.RuleBerger( @StudyId INT, @PersonId INT ) AS
BEGIN
  EXEC dbo.RulePeriodicForm @StudyId,@PersonId,'BERGER',180,2
END;