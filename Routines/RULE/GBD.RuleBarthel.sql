CREATE PROCEDURE GBD.RuleBarthel( @StudyId INT, @PersonId INT ) AS
BEGIN
  EXEC dbo.RulePeriodicForm @StudyId,@PersonId,'BARTHEL',180,2;
END;
