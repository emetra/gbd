CREATE PROCEDURE GBD.RuleQualid( @StudyId INT, @PersonId INT ) AS
BEGIN
  EXEC dbo.RulePeriodicForm @StudyId,@PersonId,'QUALID',180,2
END;