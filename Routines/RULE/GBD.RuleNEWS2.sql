CREATE PROCEDURE GBD.RuleNEWS2( @StudyId INT, @PersonId INT ) AS
BEGIN
  EXEC dbo.RulePeriodicForm @StudyId, @PersonId, 'NEWS2', 30, 2;
END
