CREATE PROCEDURE dbo.GetCaseListDruidMedium( @StudyId INT ) AS
BEGIN
  EXEC dbo.GetCaseListDruidLevel @StudyId, 2;
END