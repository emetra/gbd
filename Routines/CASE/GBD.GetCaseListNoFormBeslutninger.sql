CREATE PROCEDURE GBD.GetCaseListNoFormBeslutninger (@StudyId INT) AS
BEGIN
    EXEC dbo.GetCaseListLastForm @StudyId, 'GBD_BESLUTNINGER', 36500;
END;