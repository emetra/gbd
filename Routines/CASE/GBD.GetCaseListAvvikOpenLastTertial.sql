CREATE PROCEDURE GBD.GetCaseListAvvikOpenLastTertial( @StudyId INT ) AS
BEGIN
  DECLARE @StartAt DateTime;
  DECLARE @StopAt DateTime; 
  DECLARE @KeyDate DateTime;
  SET @KeyDate = DATEADD( MONTH, -4, GETDATE() );
  EXEC dbo.CalculateTertial @KeyDate, @StartAt OUTPUT, @StopAt OUTPUT;
  EXEC GBD.GetCaseListAvvikOpen @StudyId, @StartAt, @StopAt; 
END