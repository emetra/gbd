CREATE PROCEDURE GBD.GetCaseListLastMMSE( @StudyId INT ) AS
BEGIN
  EXEC dbo.GetCaseListLastForm @StudyId, 'MMSE_NR3'
END