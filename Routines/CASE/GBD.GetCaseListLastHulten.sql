CREATE PROCEDURE GBD.GetCaseListLastHulten( @StudyId INT ) AS
BEGIN
  EXEC GetCaseListLastForm @StudyId, 'HULTEN'
END
