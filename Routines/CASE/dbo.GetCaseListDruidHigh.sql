CREATE PROCEDURE dbo.GetCaseListDruidHigh( @StudyId INT ) AS
BEGIN
  EXEC dbo.GetCaseListDruidLevel @StudyId,4;
END