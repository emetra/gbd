CREATE PROCEDURE GBD.GetCaseListAnticholinergic( @StudyId INT ) AS 
BEGIN
  SELECT v.PersonId, v.DOB, v.FullName, v.GroupName, ot.DrugName, ac.AlertLevel,
   'Niv� ' + CONVERT(VARCHAR,AlertLevel) + ': (' + 
    ot.DrugName + COALESCE( ') ' + dd.ItemCode + ' ' + dd.ItemName, ') ikke demens.' ) AS InfoText
  FROM dbo.ViewActiveCaseListStub v
  JOIN dbo.OngoingTreatment ot ON ot.PersonId=v.PersonId
  JOIN dbo.KBAnticholinDrug ac ON ac.ATC=ot.ATC
  LEFT JOIN Diagnose.Dementia dd ON dd.PersonId=v.PersonId
  WHERE ac.AlertLevel IN ('A','B')
  ORDER BY ac.AlertLevel, FullName;
END