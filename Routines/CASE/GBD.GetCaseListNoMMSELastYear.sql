CREATE PROCEDURE GBD.GetCaseListNoMMSELastYear( @StudyId INT ) AS
BEGIN
  EXEC GetCaseListLastForm @StudyId, 'MMSE_NR3',365
END