CREATE PROCEDURE GBD.GetCaseListMUST (@StudyId INT ) AS
BEGIN
  SET NOCOUNT ON;
  EXEC GBD.UpdateMustScoreForAll @StudyId;
  SELECT vcl.PersonId,vcl.DOB,vcl.FullName,vcl.GroupName, l.LevelDesc + ': ' + a.AlertHeader AS InfoText
  FROM dbo.Alert a
    JOIN dbo.ViewActiveCaseListStub vcl ON vcl.PersonId=a.PersonId
    JOIN dbo.MetaAlertLevel l ON (l.AlertLevel=a.AlertLevel)
  WHERE ( a.StudyId=@StudyId) AND ( AlertClass='MUST' ) ORDER BY a.AlertLevel DESC,vcl.PersonId
END