CREATE PROCEDURE GBD.GetCaseListMultidose (@StudyId INT) AS
BEGIN
	SELECT v.PersonId, v.DOB, v.FullName, v.GenderId, v.GroupName, CONCAT('Antall multidosemedikamenter: ', t.Total) AS InfoText
	FROM (SELECT ot.PersonId, COUNT(ot.PackType) AS Total
		FROM dbo.OngoingTreatment ot
		WHERE ot.PackType = 'M'
		GROUP BY ot.PersonId) t
	JOIN dbo.ViewActiveCaseListStub v ON v.PersonId = t.PersonId AND v.StudyId = @StudyId
	ORDER BY v.PersonId;
END;