CREATE PROCEDURE GBD.GetCaseListLastBerger( @StudyId INT ) AS
BEGIN
  EXEC GetCaseListLastForm @StudyId, 'BERGER'
END
