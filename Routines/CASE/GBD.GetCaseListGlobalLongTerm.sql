CREATE PROCEDURE GBD.GetCaseListGlobalLongterm( @StudyId INT ) AS
BEGIN
  EXEC dbo.GetCaseListGlobalByStatusId @StudyId, 7;
END;