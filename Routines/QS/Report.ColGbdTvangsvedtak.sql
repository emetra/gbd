CREATE PROCEDURE Report.ColGbdTvangsvedtak AS
BEGIN
  SELECT 
    PersonId, 'TVANG_REST' AS VarName, 
    MAX(DATEDIFF(DD,GETDATE(),EventTime)) AS DpValue, 
    MAX(EventTime) AS MaxEventTime, 
    MAX(ClinFormId) AS MaxClinFormId
  FROM GBD.Tvangsvedtak
  GROUP BY PersonId;
END
