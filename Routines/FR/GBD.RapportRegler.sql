CREATE PROCEDURE GBD.RapportRegler( @StudyId INT ) AS
BEGIN
  SELECT    
    v.*, h.AlertLevel AS Hulten, h.AlertHeader AS HultenText,  
    w.AlertLevel AS Weight, w.AlertHeader AS WeightText, 
    bp.AlertLevel AS BP, BP.AlertHeader AS BPText,    
    lab.AlertLevel AS Lab, Lab.AlertHeader AS LabText,    
    k.AlertLevel AS Kost, k.AlertHeader AS KostText, 
    sc.LastRuleExecute                                                    
  FROM dbo.ViewActiveCaseListStub v    
  JOIN dbo.StudCase sc ON sc.StudyId = v.StudyId AND sc.PersonId = v.PersonId                                                                  
    LEFT OUTER JOIN dbo.Alert h ON h.StudyId = v.StudyId AND h.PersonId = v.PersonId AND h.AlertClass='HULTEN'  
    LEFT OUTER JOIN dbo.Alert w ON w.StudyId = v.StudyId AND w.PersonId = v.PersonId AND w.AlertClass = 'WEIGHT30D'  
    LEFT OUTER JOIN dbo.Alert bp ON bp.StudyId = v.StudyId AND bp.PersonId = v.PersonId AND bp.AlertClass = 'SBP_UNSPEC30'  
    LEFT OUTER JOIN dbo.Alert lab ON lab.StudyId = v.StudyId AND lab.PersonId = v.PersonId AND lab.AlertClass = 'LAB'  
    LEFT OUTER JOIN dbo.Alert k ON k.StudyId = v.StudyId AND k.PersonId = v.PersonId AND k.AlertClass = 'KostSamtale'    
    WHERE v.StudyId = @StudyId
  ORDER BY v.GroupName, v.FullName;
END     