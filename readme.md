Om GBD
======

GBD er en forkortelse for "Geriatrisk Basis Datasett".
Her finner du dokumenter som er spesifikke for GBD.
Disse filene brukes i FastTrak i Bergen Kommune.

Mapper
======
 
  * **Documents** - innholder dokumenter som ikke er brukt direkte av systemet.
  * **Images** - Inneholder bilder som inngår i dokumentasjon.
  * **MDS** - Filer knyttet til Minimum Dataset (sykehjemsdokumentasjon fra USA).
  * **Patient** - Rapporter (\*.fr3) og oversikter (\*.html) for en enkeltpasient.
  * **Population** - Rapporter (\*.fr3) som inneholder flere pasienter.
  * **Screenshots** - Skjermbilder som kan inngå i dokumentasjon eller presentasjoner.


Forsiden
========

I roten ligger filen [index.html](http://downloads.fasttrak.no/GBD/index.html).  Dette er forsiden som viser for brukerne.
Websidene ligger på [nedlastingsserveren](http://downloads.fasttrak.no) og oppdateres automatisk hver gang det gjøres en push mot master på bitbucket.

Nedlasting
==========

IKT Drift får automatisk tilsendt epost med link til oppdatering og instruksjoner fra og august 2016.

Installasjon
============
Pakk ut nedlastet zip-fil og kopier inn med overskriving av eksisterende filer. 
Kjør deretter `\bin\FastTrakUpdate.exe` og kryss av for alle tabeller før kjøring.
Husk å krysse av for "Lokale filer".

Bergen, 15. november 2017

Magne Rekdal,  
Medisinsk Rådgiver